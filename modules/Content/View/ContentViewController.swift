//
//  ContentContentViewController.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import UIKit

class ContentViewController: UIViewController, ContentViewInput, ContentTableViewDataSourceDelegate, UIViewControllerTransitioningDelegate {
    let kEstimatedRowHeight: CGFloat = 77
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var contentTableView: UITableView!
    
    var output: ContentViewOutput!
    var contentDataSource: ContentTableViewDataSource!
    var sidePresentAnimationController = SidePresentAnimationController()
    var sideDismissAnimationController = SideDismissAnimationController()
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()

        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        output.fetchTasks()
    }
    
    // MARK: setup
    
    func setup() {
        setupTableView()
        setupTransition()
    }
    
    func setupTransition() {
        transitioningDelegate = self
    }
    
    func setupTableView() {
        contentDataSource = ContentTableViewDataSource()
        contentDataSource.delegate = self
        
        contentTableView.dataSource = contentDataSource
        contentTableView.rowHeight = UITableViewAutomaticDimension
        contentTableView.estimatedRowHeight = kEstimatedRowHeight
    }

    // MARK: ContentViewInput
    
    func setupInitialState() {}
    
    func tasksFetchingDidFinished(tasks: [AppTask]) {
        emptyView.isHidden = tasks.count > 0
        contentTableView.isHidden = !(tasks.count > 0)
        
        contentDataSource.items = tasks
        contentTableView.reloadData()
    }
    
    func taskDidRemove() {
        output.fetchTasks()
    }
    
    // MARK: actions
    
    @IBAction func createTaskButtonDidTap(_ sender: Any) {
        output.openEditScreen(fromView: self)
    }
    
    @IBAction func calendarButtonDidTap(_ sender: Any) {
        output.openCalendarScreen(fromView: self)
    }
    
    // MARK: ContentTableViewDataSourceDelegate
    
    func taskWillBeRemoved(task: AppTask) {
        output.removeTask(task: task)
    }
    
    // MARK: UIViewControllerTransitioningDelegate
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return sidePresentAnimationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return sideDismissAnimationController
    }
    
    // MARK: segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ViewControllerIDs.calendarScreen {
            let toViewController = segue.destination as UIViewController
            toViewController.transitioningDelegate = self
        }
    }
}
