//
//  CalendarCalendarConfigurator.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import UIKit

class CalendarModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? CalendarViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: CalendarViewController) {

        let router = CalendarRouter()

        let presenter = CalendarPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = CalendarInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
