//
//  ContentContentPresenter.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

class ContentPresenter: ContentModuleInput, ContentViewOutput, ContentInteractorOutput {
    weak var view: ContentViewInput!
    var interactor: ContentInteractorInput!
    var router: ContentRouterInput!

    func viewIsReady() {}
    
    func openEditScreen(fromView: ContentViewInput) {
        router.openEditScreen(fromView: fromView)
    }
    
    func tasksFetchingDidFinished(tasks: [AppTask]) {
        view.tasksFetchingDidFinished(tasks: tasks)
    }
    
    func fetchTasks() {
        interactor.fetchTasks()
    }
    
    func removeTask(task: AppTask) {
        interactor.removeTask(task: task)
    }
    
    func taskDidRemove() {
        view.taskDidRemove()
    }
    
    func openCalendarScreen(fromView: ContentViewInput) {
        router.openCalendarScreen(fromView: fromView)
    }
}
