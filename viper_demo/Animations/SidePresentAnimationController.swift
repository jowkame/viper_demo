//
//  SidePresentAnimationController.swift
//  viper_demo
//
//  Created by jowkame on 9/18/16.
//  Copyright © 2016 jowkame. All rights reserved.
//

import UIKit

let kSidePresentAnimationDuration: TimeInterval = 0.35

class SidePresentAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    // MARK: - UIViewControllerAnimatedTransitioning
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return kSidePresentAnimationDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let finalFrameForVC = transitionContext.finalFrame(for: toViewController)
        let containerView = transitionContext.containerView
        let bounds = UIScreen.main.bounds
        toViewController.view.frame = finalFrameForVC.offsetBy(dx: -bounds.size.width, dy: 0)
        containerView.addSubview(toViewController.view)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: { 
            toViewController.view.frame = finalFrameForVC
            fromViewController.view.frame = finalFrameForVC.offsetBy(dx: bounds.size.width, dy: 0)
        }) { (completion) in
            transitionContext.completeTransition(true)
        }
    }
}
