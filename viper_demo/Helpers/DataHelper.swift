//
//  DataHelper.swift
//  viper_demo
//
//  Created by jowkame on 4/23/18.
//  Copyright © 2018 rubygarage. All rights reserved.
//

import MagicalRecord

class DataHelper {
    class func saveTask(appTask: AppTask, completion: @escaping (_ success: Bool) -> ()) {
        let context = NSManagedObjectContext.mr_default()
        
        context.mr_save({ (context) in
            let task = Task.mr_createEntity(in: context)
            task?.title = appTask.title
            task?.details = appTask.details
            task?.colorAlpha = Float(CGFloat.random(lower: 0.6, 1))
            task?.date = appTask.date
        }) { (success, error) in
            completion(success)
        }
    }
    
    class func fetchTasks(completion: @escaping (_ tasks: [AppTask]) -> ()) {
        let parentContext = NSManagedObjectContext.mr_default()
        let privateContext = NSManagedObjectContext.mr_context(withParent: parentContext)
        privateContext.perform {
            let tasks = Task.mr_findAll(in: privateContext) as! [Task]
            
            var appTasks = [AppTask]()
            
            for task in tasks {
                let appTask = AppTask()
                appTask.update(withTask: task)
                appTasks.append(appTask)
            }
            
            DispatchQueue.main.async {
                completion(appTasks)
            }
        }
    }
    
    class func deleteTask(task: AppTask, completion: @escaping (_ success: Bool) -> ()) {
        let context = NSManagedObjectContext.mr_default()
        
        let url = URL(string: task.objId)
        let objId = context.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: url!)
        
        do {
            let object = try context.existingObject(with: objId!)
            
            object.mr_deleteEntity(in: context)
            context.mr_saveToPersistentStore(completion: { (success, error) in
                DispatchQueue.main.async {
                    completion(true)
                }
            })
        } catch {
            completion(false)
        }
    }
}
