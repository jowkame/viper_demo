//
//  Color.swift
//  viper_demo
//
//  Created by jowkame on 4/24/18.
//  Copyright © 2018 rubygarage. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
    
    class func defaultBlueColor() -> UIColor {
        return UIColor(red: 105, green: 153, blue: 183)
    }
}
