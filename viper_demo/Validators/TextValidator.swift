//
//  TextValidator.swift
//  viper_demo
//
//  Created by jowkame on 4/23/18.
//  Copyright © 2018 rubygarage. All rights reserved.
//

import Foundation

class TextValidator {
    class func isTaskDataValid(title: String, details: String) -> Bool {
        return !title.isEmpty && !details.isEmpty
    }
}
