//
//  EditEditViewInput.swift
//  viper_demo
//
//  Created by jowkame on 23/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

protocol EditViewInput: class {

    /**
        @author jowkame
        Setup initial state of the view
    */

    func setupInitialState()
    func taskCreationDidFinish()
}
