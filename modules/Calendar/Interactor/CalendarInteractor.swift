//
//  CalendarCalendarInteractor.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

class CalendarInteractor: CalendarInteractorInput {

    weak var output: CalendarInteractorOutput!
    
    func fetchTasks() {
        DataHelper.fetchTasks { [weak self] (tasks) in
            self?.output.tasksDidFetch(tasks: tasks)
        }
    }
}
