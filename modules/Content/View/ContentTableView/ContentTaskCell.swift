//
//  ContentTaskCell.swift
//  viper_demo
//
//  Created by jowkame on 4/23/18.
//  Copyright © 2018 rubygarage. All rights reserved.
//

import UIKit

class ContentTaskCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var colorView: UIView!

    func configure(task: AppTask) {
        selectionStyle = .none
        
        titleLabel.text = task.title
        detailLabel.text = task.details
        colorView.alpha = task.colorAlpha
    }
}
