//
//  EditEditPresenter.swift
//  viper_demo
//
//  Created by jowkame on 23/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

class EditPresenter: EditModuleInput, EditViewOutput, EditInteractorOutput {
    weak var view: EditViewInput!
    var interactor: EditInteractorInput!
    var router: EditRouterInput!

    func viewIsReady() {

    }
    
    func saveNewTask(task: AppTask) {
        interactor.saveNewTask(task: task)
    }
    
    func finishTaskCreation() {
        view.taskCreationDidFinish()
    }
}
