//
//  ContentContentInteractor.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

class ContentInteractor: ContentInteractorInput {
    weak var output: ContentInteractorOutput!
    
    func fetchTasks() {
        DataHelper.fetchTasks { [weak self] (tasks) in
            self?.output.tasksFetchingDidFinished(tasks: tasks)
        }
    }
    
    func removeTask(task: AppTask) {
        DataHelper.deleteTask(task: task) { [weak self] (success) in
            self?.output.taskDidRemove()
        }
    }
}
