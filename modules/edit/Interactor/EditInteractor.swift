//
//  EditEditInteractor.swift
//  viper_demo
//
//  Created by jowkame on 23/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import MagicalRecord

class EditInteractor: EditInteractorInput {
    weak var output: EditInteractorOutput!

    func saveNewTask(task: AppTask) {
        DataHelper.saveTask(appTask: task) { [weak self] (success) in
            self?.output.finishTaskCreation()
        }
    }
}
