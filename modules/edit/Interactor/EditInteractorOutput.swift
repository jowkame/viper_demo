//
//  EditEditInteractorOutput.swift
//  viper_demo
//
//  Created by jowkame on 23/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import Foundation

protocol EditInteractorOutput: class {
    func finishTaskCreation()
}
