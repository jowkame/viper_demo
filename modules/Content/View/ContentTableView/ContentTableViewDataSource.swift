//
//  ContentTableViewDataSource.swift
//  viper_demo
//
//  Created by jowkame on 4/23/18.
//  Copyright © 2018 rubygarage. All rights reserved.
//

import UIKit

protocol ContentTableViewDataSourceDelegate {
    func taskWillBeRemoved(task: AppTask)
}

class ContentTableViewDataSource: NSObject, UITableViewDataSource {
    var items = [AppTask]()
    var delegate: ContentTableViewDataSourceDelegate! = nil
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellsIDs.task, for: indexPath)
        
        let task = items[indexPath.row]
        (cell as! ContentTaskCell).configure(task: task)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let task = items[indexPath.row]
            items.remove(at: indexPath.row)
            
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .top)
            tableView.endUpdates()

            delegate?.taskWillBeRemoved(task: task)
        }
    }
}
