//
//  SideDismissAnimationController.swift
//  viper_demo
//
//  Created by jowkame on 9/18/16.
//  Copyright © 2016 jowkame. All rights reserved.
//

import UIKit

let kSideDismissAnimationDuration: TimeInterval = 0.35

class SideDismissAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    var animationTransitionMoment: (() -> ())! = nil
    var animationTransitionCompletion: (() -> ())! = nil

    // MARK: - UIViewControllerAnimatedTransitioning
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return kSideDismissAnimationDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let finalFrameForVC = transitionContext.finalFrame(for: toViewController)
        let containerView = transitionContext.containerView
        containerView.addSubview(toViewController.view)
        containerView.sendSubview(toBack: toViewController.view)
        let bounds = UIScreen.main.bounds
        
        self.animationTransitionMoment?()

        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            toViewController.view.frame = finalFrameForVC
            fromViewController.view.frame = finalFrameForVC.offsetBy(dx: -bounds.size.width, dy: 0)
        }, completion: {
            finished in
            transitionContext.completeTransition(true)
            self.animationTransitionCompletion?()
        })
    }
}
