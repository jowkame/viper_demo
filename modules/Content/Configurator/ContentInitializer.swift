//
//  ContentContentInitializer.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import UIKit

class ContentModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var contentViewController: ContentViewController!

    override func awakeFromNib() {

        let configurator = ContentModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: contentViewController)
    }

}
