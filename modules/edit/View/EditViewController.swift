//
//  EditEditViewController.swift
//  viper_demo
//
//  Created by jowkame on 23/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import UIKit
import DatePickerDialog

class EditViewController: UIViewController, EditViewInput, UITextViewDelegate, UITextFieldDelegate {
    private let kBorderWidth: CGFloat = 1
    private let kBorderAlpha: CGFloat = 0.25
    private let kBorderCornerRadius: CGFloat = 6
    private let kKeyboardAnimationDuraton: TimeInterval = 0.8
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var detailsTextView: UITextView!
    @IBOutlet weak var saveTaskButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var activeTextView: UITextView!
    var output: EditViewOutput!

    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        subscribeToKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        unsubscribeFromKeyboard()
    }
    
    // MARK: setup
    
    func setup() {
        setupTitleView()
        setupDetailsTextView()
    }
    
    func setupTitleView() {
        titleTextField.delegate = self
    }
    
    func setupDetailsTextView() {
        detailsTextView.layer.borderWidth = kBorderWidth
        detailsTextView.layer.borderColor = UIColor.lightGray.withAlphaComponent(kBorderAlpha).cgColor
        detailsTextView.layer.cornerRadius = kBorderCornerRadius
        detailsTextView.delegate = self
    }

    // MARK: EditViewInput
    
    func setupInitialState() {}
    
    // MARK: actions
    
    @IBAction func saveTaskButtonDidTap(_ sender: Any) {
        DatePickerDialog().show("Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) { [weak self]
            (date) -> () in
            if let dt = date {
                let task = AppTask()
                task.details = (self?.detailsTextView.text)!
                task.title = (self?.titleTextField.text)!
                task.date = dt
                self?.output.saveNewTask(task: task)
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfoDict = notification.userInfo, let keyboardFrameValue = userInfoDict[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardFrame = keyboardFrameValue.cgRectValue
            if self.bottomConstraint.constant == 0 {
                UIView.animate(withDuration: kKeyboardAnimationDuraton) {
                    self.bottomConstraint.constant = keyboardFrame.height
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: kKeyboardAnimationDuraton) {
            self.bottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func taskCreationDidFinish() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: subscription
    
    func subscribeToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(EditViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unsubscribeFromKeyboard() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK: text field delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            saveTaskButton.isEnabled = TextValidator.isTaskDataValid(title: updatedText, details: detailsTextView.text)
        }
        return true
    }
    
    // MARK: text view delegate
    
    func textViewDidChange(_ textView: UITextView) {
        saveTaskButton.isEnabled = TextValidator.isTaskDataValid(title: titleTextField.text!, details: detailsTextView.text)
    }
}
