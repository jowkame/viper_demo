//
//  SettingsSettingsViewInput.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

protocol SettingsViewInput: class {

    /**
        @author jowkame
        Setup initial state of the view
    */

    func setupInitialState()
}
