//
//  EditEditConfigurator.swift
//  viper_demo
//
//  Created by jowkame on 23/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import UIKit

class EditModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? EditViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: EditViewController) {

        let router = EditRouter()

        let presenter = EditPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = EditInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
