//
//  EditEditInitializer.swift
//  viper_demo
//
//  Created by jowkame on 23/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import UIKit

class EditModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var editViewController: EditViewController!

    override func awakeFromNib() {

        let configurator = EditModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: editViewController)
    }

}
