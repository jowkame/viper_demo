
//
//  Constant.swift
//  viper_demo
//
//  Created by jowkame on 4/23/18.
//  Copyright © 2018 rubygarage. All rights reserved.
//

import Foundation

class ViewControllerIDs {
    static var editScreen = "EditScreenSegue"
    static var calendarScreen = "CalendarScreenSegue"
}

class TableViewCellsIDs {
    static var task = "ContentTaskCellIdentifier"
}
