//
//  ContentContentRouter.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import UIKit

class ContentRouter: ContentRouterInput {
    func openEditScreen(fromView: ContentViewInput) {
        openViewController(segueIdentifier: ViewControllerIDs.editScreen, fromView: fromView)
    }
    
    func openCalendarScreen(fromView: ContentViewInput) {
        openViewController(segueIdentifier: ViewControllerIDs.calendarScreen, fromView: fromView)
    }
    
    func openViewController(segueIdentifier: String, fromView: ContentViewInput) {
        (fromView as! UIViewController).performSegue(withIdentifier: segueIdentifier, sender: nil)
    }
}
