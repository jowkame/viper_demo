//
//  AppTask.swift
//  viper_demo
//
//  Created by jowkame on 4/23/18.
//  Copyright © 2018 rubygarage. All rights reserved.
//

import UIKit

class AppTask: AppModel {
    var title = String()
    var details = String()
    var colorAlpha: CGFloat = 1
    var objId = String()
    var date = Date()
    
    func update(withTask: Task) {
        title = withTask.title!
        details = withTask.details!
        colorAlpha = CGFloat(withTask.colorAlpha)
        date = withTask.date!
        objId = withTask.objectID.uriRepresentation().absoluteString
    }
}
