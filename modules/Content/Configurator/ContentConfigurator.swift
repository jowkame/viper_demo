//
//  ContentContentConfigurator.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import UIKit

class ContentModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? ContentViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ContentViewController) {
        let router = ContentRouter()
        
        let presenter = ContentPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ContentInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }
}
