//
//  AppDelegate.swift
//  viper_demo
//
//  Created by jowkame on 4/20/18.
//  Copyright © 2018 rubygarage. All rights reserved.
//

import UIKit
import MagicalRecord

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setupMagicalRecord()
        
        return true
    }
    
    func setupMagicalRecord() {
        MagicalRecord.setupAutoMigratingCoreDataStack()
    }
}
