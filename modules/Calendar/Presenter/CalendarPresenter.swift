//
//  CalendarCalendarPresenter.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

class CalendarPresenter: CalendarModuleInput, CalendarViewOutput, CalendarInteractorOutput {
    weak var view: CalendarViewInput!
    var interactor: CalendarInteractorInput!
    var router: CalendarRouterInput!

    func viewIsReady() {}
    
    func fetchTasks() {
        interactor.fetchTasks()
    }
    
    func tasksDidFetch(tasks: [AppTask]) {
        view.tasksDidFetch(tasks: tasks)
    }
}
