//
//  CalendarCalendarViewController.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import UIKit
import CVCalendar
import DateToolsSwift

class CalendarViewController: UIViewController, CalendarViewInput {
    @IBOutlet weak var calendarView: CVCalendarView!
    @IBOutlet weak var calendarMenuView: CVCalendarMenuView!
    @IBOutlet weak var mainDateLabel: UILabel!
    
    var output: CalendarViewOutput!
    var tasksDates = [Date]()
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        output.fetchTasks()
    }

    // MARK: adjust views
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        calendarMenuView.commitMenuViewUpdate()
        calendarView.commitCalendarViewUpdate()
    }
    
    // MARK: - setup
    
    func setupCalendar() {
        let calendarAppearance = Appearance()
        calendarAppearance.delegate = self
        calendarView.appearance = calendarAppearance
        calendarMenuView.delegate = self
        calendarView.calendarDelegate = self
    }

    // MARK: CalendarViewInput
    
    func setupInitialState() {}
    
    // MARK: actions
    
    @IBAction func doneButtonDidTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func tasksDidFetch(tasks: [AppTask]) {
        tasksDates = tasks.map({$0.date})
        
        setupCalendar()
    }
}

// MARK: - CVCalendarViewDelegate
extension CalendarViewController: CVCalendarViewDelegate {
    func presentationMode() -> CalendarMode {
        return .monthView
    }
    
    func firstWeekday() -> Weekday {
        return .monday
    }
}

// MARK: - CVCalendarViewAppearanceDelegate
extension CalendarViewController: CVCalendarViewAppearanceDelegate {
    func dayLabelColor(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIColor? {
        return (status == .selected) ? UIColor.white : UIColor.defaultBlueColor()
    }
    
    func dayLabelWeekdaySelectedBackgroundColor() -> UIColor {
        return UIColor.defaultBlueColor()
    }
    
    func dayLabelPresentWeekdaySelectedBackgroundColor() -> UIColor {
        return UIColor.defaultBlueColor()
    }
    
    func didShowNextMonthView(_ date: Date) {
        self.mainDateLabel.text = date.format(with: DateFormatter.Style.long)
    }

    func didShowPreviousMonthView(_ date: Date) {
        self.mainDateLabel.text = date.format(with: DateFormatter.Style.long)
    }

    func didSelectDayView(_ dayView: DayView, animationDidFinish: Bool) {
        let date = dayView.date.convertedDate(calendar: Calendar.current)!
        self.mainDateLabel.text = date.format(with: DateFormatter.Style.long)
    }
    
    func dotMarker(shouldShowOnDayView dayView: DayView) -> Bool {
        if let cvDate = dayView.date {
            let date = cvDate.convertedDate()
            
            let filteredDates = tasksDates.filter({ (checkDate) -> Bool in
                return date!.isSameDay(date: checkDate)
            })

            return filteredDates.first != nil
        }
        return false
    }
    
    func dotMarkerColor() -> UIColor {
        return UIColor.defaultBlueColor()
    }
    
    func dotMarker(colorOnDayView dayView: DayView) -> [UIColor] {
        return [UIColor.blue]
    }
}

extension CalendarViewController: CVCalendarMenuViewDelegate {
    func dayOfWeekTextColor() -> UIColor {
        return UIColor.defaultBlueColor()
    }
}
