//
//  ContentContentInteractorInput.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import Foundation

protocol ContentInteractorInput {
    func fetchTasks()
    func removeTask(task: AppTask)
}
