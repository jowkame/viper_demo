//
//  EditEditViewOutput.swift
//  viper_demo
//
//  Created by jowkame on 23/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

import UIKit

protocol EditViewOutput {

    /**
        @author jowkame
        Notify presenter that view is ready
    */

    func viewIsReady()
    func saveNewTask(task: AppTask)
}
