//
//  CalendarCalendarViewOutput.swift
//  viper_demo
//
//  Created by jowkame on 20/04/2018.
//  Copyright © 2018 jowkame. All rights reserved.
//

protocol CalendarViewOutput {

    /**
        @author jowkame
        Notify presenter that view is ready
    */

    func viewIsReady()
    func fetchTasks()
}
